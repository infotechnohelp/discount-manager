<?php

class Entity
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function get(string $key)
    {
        return $this->data[$key];
    }
}


class DiscountManager
{
    /**
     * @var Entity
     */
    private $Product;

    private $globalDiscounts;

    private $personalDiscounts;

    public function __construct(
        Entity $Product,
        array /*ResultSet*/ $availableGlobalDiscounts = null,
        array /*ResultSet*/ $availablePersonalDiscounts = null
    )
    {
        $this->setProduct($Product);
        $this->globalDiscounts = $availableGlobalDiscounts;
        $this->personalDiscounts = $availablePersonalDiscounts;
    }

    public function setProduct(Entity $Product)
    {
        $this->Product = $Product;
    }

    private function getProduct()
    {
        return $this->Product;
    }

    public function setGlobalDiscounts($availableGlobalDiscounts = null)
    {
        $this->globalDiscounts = $availableGlobalDiscounts;
    }

    private function getGlobalDiscounts()
    {
        return $this->globalDiscounts;
    }

    public function setPersonalDiscounts($availablePersonalDiscounts = null)
    {
        $this->personalDiscounts = $availablePersonalDiscounts;
    }

    private function getPersonalDiscounts()
    {
        return $this->personalDiscounts;
    }


    private function findDiscount(bool $personal = false, int $bookTypeId = null, array $bookGenreIds = [], array $bookGroupIds = [])
    {
        $result = [];

        $discounts = ($personal) ? $this->getPersonalDiscounts() : $this->getGlobalDiscounts();

        foreach ($discounts as $discount) {

            if (!empty($bookTypeId) && !in_array($bookTypeId, $discount->get('book_instance_type_ids'), true)) {
                continue;
            }

            if (empty($discount->get('book_instance_genre_ids')) && empty($discount->get('book_instance_group_ids'))) {
                $result[] = $discount;
                continue;
            }

            $bookGenreMatches = false;

            foreach ($bookGenreIds as $bookGenreId) {
                if (in_array($bookGenreId, $discount->get('book_instance_genre_ids'), true)) {
                    $bookGenreMatches = true;
                    break;
                }
            }

            $bookGroupMatches = false;

            foreach ($bookGroupIds as $bookGroupId) {
                if (in_array($bookGroupId, $discount->get('book_instance_group_ids'), true)) {
                    $bookGroupMatches = true;
                    break;
                }
            }

            if (!empty($discount->get('book_instance_genre_ids')) && !$bookGenreMatches) {
                continue;
            }

            if (!empty($discount->get('book_instance_group_ids')) && !$bookGroupMatches) {
                continue;
            }

            $result[] = $discount;
        }

        return $result;
    }

    private function findGlobalDiscounts(int $bookTypeId = null, array $bookGenreIds = [], array $bookGroupIds = [])
    {
        return $this->findDiscount(false, $bookTypeId, $bookGenreIds, $bookGroupIds);
    }

    private function findPersonalDiscounts(int $bookTypeId = null, array $bookGenreIds = [], array $bookGroupIds = [])
    {
        return $this->findDiscount(true, $bookTypeId, $bookGenreIds, $bookGroupIds);
    }


    private function getBestDiscount(array $discounts = [])
    {
        if (empty($discounts)) {
            return null;
        }

        $bestDiscount = new Entity(['percent' => 0]);

        foreach ($discounts as $discount) {
            if ($discount->get('percent') > $bestDiscount->get('percent')) {
                $bestDiscount = $discount;
            }
        }

        return $bestDiscount;
    }

    private function calculateDiscountedPrice($discount, $additionalDiscount = null)
    {
        $price = $this->getProduct()->get('price');

        if (empty($additionalDiscount)) {
            return round(
                $price - ($price * $discount->get('percent') / 100),
                2);
        }

        return round(
            $price - ($price * $discount->get('percent') / 100) - ($price * $additionalDiscount->get('percent') / 100),
            2);
    }


    public function getPrice()
    {

        $globalDiscounts = (!empty($this->getGlobalDiscounts())) ?
            $this->findGlobalDiscounts(
                $this->Product->get('book_instance_type_id'),
                $this->Product->get('book_instance_genre_ids'),
                $this->Product->get('book_instance_group_ids')
            ) :
            [];

        $bestGlobalDiscount = $this->getBestDiscount($globalDiscounts);


        $personalDiscounts = (!empty($this->getPersonalDiscounts())) ?
            $this->findPersonalDiscounts(
                $this->Product->get('book_instance_type_id'),
                $this->Product->get('book_instance_genre_ids'),
                $this->Product->get('book_instance_group_ids')
            ) :
            [];

        $bestPersonalDiscount = $this->getBestDiscount($personalDiscounts);



        if (!empty($bestGlobalDiscount)) {
            if (empty($bestPersonalDiscount)) {
                return $this->calculateDiscountedPrice($bestGlobalDiscount);
            }

            if ($bestPersonalDiscount->get('additional')) {
                return $this->calculateDiscountedPrice($bestGlobalDiscount, $bestPersonalDiscount);
            }

            $bestDiscount = $this->getBestDiscount([$bestGlobalDiscount, $bestPersonalDiscount]);

            return $this->calculateDiscountedPrice($bestDiscount);
        }

        if(!empty($bestPersonalDiscount) && empty($bestGlobalDiscount)){
            return $this->calculateDiscountedPrice($bestPersonalDiscount);
        }


        return $this->getProduct()->get('price');
    }
}


// TEST


$ProductEntity = new Entity(
    [
        'price' => 10.99,
        'book_instance_type_id' => 1,
        'book_instance_group_ids' => [1, 2],
        'book_instance_genre_ids' => [1, 2],
    ]
);


$DiscountManager = new DiscountManager($ProductEntity);

var_dump($DiscountManager->getPrice());

$availableGlobalDiscounts =
    [
        new Entity(
            [
                'percent' => 15.3,
                'book_instance_type_ids' => [1],
                'book_instance_group_ids' => [2],
                'book_instance_genre_ids' => [1],
            ]
        )

    ];

$DiscountManager->setGlobalDiscounts($availableGlobalDiscounts);

$availableGlobalDiscounts =
    [
        new Entity(
            [
                'percent' => 21,
                'book_instance_type_ids' => [1],
                'book_instance_group_ids' => [2],
                'book_instance_genre_ids' => [1],
            ]
        )

    ];

$DiscountManager->setGlobalDiscounts($availableGlobalDiscounts);

var_dump($DiscountManager->getPrice());


$availablePersonalDiscounts =
    [
        new Entity(
            [
                'percent' => 15.3,
                'additional' => false,
                'book_instance_type_ids' => [1],
                'book_instance_group_ids' => [2],
                'book_instance_genre_ids' => [1],
            ]
        )

    ];

$DiscountManager->setPersonalDiscounts($availablePersonalDiscounts);

var_dump($DiscountManager->getPrice());
