<?php

class Entity
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function get(string $key)
    {
        return $this->data[$key];
    }
}


class DiscountManager
{
    /**
     * @var Entity
     */
    private $Product;

    private $globalDiscounts;

    private $personalDiscounts;

    public function __construct(
        Entity $Product,
        array /*ResultSet*/ $availableGlobalDiscounts = null,
        array /*ResultSet*/ $availablePersonalDiscounts = null
    )
    {
        $this->setProduct($Product);
        $this->globalDiscounts = $availableGlobalDiscounts;
        $this->personalDiscounts = $availablePersonalDiscounts;
    }

    public function setProduct(Entity $Product)
    {
        $this->Product = $Product;
    }

    private function getProduct()
    {
        return $this->Product;
    }

    public function setGlobalDiscounts($availableGlobalDiscounts = null)
    {
        $this->globalDiscounts = $availableGlobalDiscounts;
    }

    private function getGlobalDiscounts()
    {
        return $this->globalDiscounts;
    }

    public function setPersonalDiscounts($availablePersonalDiscounts = null)
    {
        $this->personalDiscounts = $availablePersonalDiscounts;
    }

    private function getPersonalDiscounts()
    {
        return $this->personalDiscounts;
    }

    private function findDiscount(
        bool $personal,
        int $bookTypeId,
        int $bookGroupId = null,
        int $bookGenreId = null
    )
    {
        // var_dump([$bookTypeId, $bookGroupId, $bookGenreId]);

        $result = null;
        $fullMatch = null;
        $partialMatch = null;

        $discounts = ($personal) ? $this->getPersonalDiscounts() : $this->getGlobalDiscounts();

        foreach ($discounts as $discount) {

            if (
                $discount->get('book_instance_type_id') === $bookTypeId &&
                $discount->get('book_instance_group_id') === $bookGroupId
                ||
                $discount->get('book_instance_type_id') === $bookTypeId &&
                $discount->get('book_instance_genre_id') === $bookGenreId

            ) {
                $fullMatch = $discount;
            }

            if ($discount->get('book_instance_type_id') === $bookTypeId) {
                $partialMatch = $discount;
            }
        }

        $result = ($fullMatch !== null) ? $fullMatch : $partialMatch;

        return $result;
    }

    public function getPrice()
    {
        if ($this->getProduct()->get('discounted_price') !== null) {
            return $this->getProduct()->get('discounted_price');
        }

        $price = $this->getProduct()->get('price');

        $globalDiscount = null;
        $personalDiscount = null;
        $personalDiscountIsAdditional = false;


        if (!empty($this->getPersonalDiscounts())) {

            $discount = $this->findDiscount(
                true,
                $this->Product->get('book_instance_type_id'),
                $this->Product->get('book_instance_group_id'),
                $this->Product->get('book_instance_genre_id')
            );

            if ($discount->get('additional')) {
                $personalDiscountIsAdditional = true;
                //return round($price - ($price * $discount->get('percent') / 100), 2);
            }

            $personalDiscount = $discount;
        }

        if (!empty($this->getGlobalDiscounts())) {

            $discount = $this->findDiscount(
                false,
                $this->Product->get('book_instance_type_id'),
                $this->Product->get('book_instance_group_id'),
                $this->Product->get('book_instance_genre_id')
            );

            $globalDiscount = $discount;
        }

        if ($globalDiscount === null && $personalDiscount === null) {
            return $price;
        }

        if ($globalDiscount !== null && $personalDiscount === null) {
            return round($price - ($price * $globalDiscount->get('percent') / 100), 2);
        }

        if ($globalDiscount === null && $personalDiscount !== null) {
            return round($price - ($price * $personalDiscount->get('percent') / 100), 2);
        }

        if ($globalDiscount !== null && $personalDiscount !== null) {

            if (!$personalDiscountIsAdditional) {

                if ($personalDiscount->get('percent') < $globalDiscount->get('percent')) {
                    return round($price - ($price * $globalDiscount->get('percent') / 100), 2);
                }

                return round($price - ($price * $personalDiscount->get('percent') / 100), 2);
            }

            return round(
                $price - ($price * $globalDiscount->get('percent') / 100) - ($price * $personalDiscount->get('percent') / 100),
                2);
        }


        return null;
    }
}


$ProductEntity = new Entity(
    [
        'id' => 1,
        'price' => 10.99,
        'discounted_price' => 9.99,
        'book_instance_type_id' => 1,
        'book_instance_group_id' => 1,
        'book_instance_genre_id' => 1,
    ]
);


$DiscountManager = new DiscountManager($ProductEntity);

var_dump($DiscountManager->getPrice());


$ProductEntity = new Entity(
    [
        'id' => 1,
        'price' => 10.99,
        'discounted_price' => null,
        'book_instance_type_id' => 1,
        'book_instance_group_id' => 1,
        'book_instance_genre_id' => 1,
    ]
);

$DiscountManager->setProduct($ProductEntity);

var_dump($DiscountManager->getPrice());


// Get them from DB by User Group

$availablePersonalDiscounts =
    [
        new Entity(
            [
                'percent' => 9.7,
                'book_instance_type_id' => 1,
                'book_instance_group_id' => null,
                'book_instance_genre_id' => null,
                'additional' => false,
            ]
        )

    ];

$DiscountManager->setPersonalDiscounts($availablePersonalDiscounts);

var_dump($DiscountManager->getPrice());


$availablePersonalDiscounts =
    [
        new Entity(
            [
                'percent' => 9.7,
                'book_instance_type_id' => 1,
                'book_instance_group_id' => null,
                'book_instance_genre_id' => null,
                'additional' => true,
            ]
        )

    ];

$DiscountManager->setPersonalDiscounts($availablePersonalDiscounts);

var_dump($DiscountManager->getPrice());


$availableGlobalDiscounts =
    [
        new Entity(
            [
                'percent' => 15.3,
                'book_instance_type_id' => 1,
                'book_instance_group_id' => null,
                'book_instance_genre_id' => null,
            ]
        )

    ];

$DiscountManager->setGlobalDiscounts($availableGlobalDiscounts);

var_dump($DiscountManager->getPrice());


$DiscountManager->setPersonalDiscounts(null);

var_dump($DiscountManager->getPrice());



$DiscountManager = new DiscountManager($ProductEntity, $availableGlobalDiscounts, [

    new Entity(
        [
            'percent' => 10,
            'book_instance_type_id' => 1,
            'book_instance_group_id' => null,
            'book_instance_genre_id' => null,
        ]
    )

]);